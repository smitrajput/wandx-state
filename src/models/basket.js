const mongoose = require("mongoose");

const basketSchema = new mongoose.Schema({
  userAddress: {
    type: String,
    required: true
  },
  basketCreationHash: {
    type: String,
    required: true
  },
  tokens: [
    {
      type: Object,
      tokenSymbol: String,
      amount: Number,
      txType: {
        type: Object,
        txTypeName: String,
        signature: String,
        status: {
          type: Object,
          statusName: String,
          statusHash: String
        }
      }
    }
  ]
});

const Basket = mongoose.model("basket", basketSchema);

module.exports = Basket;
