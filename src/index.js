const express = require("express");
require("./db/mongoose");
const Basket = require("./models/basket");
const { MongoClient, ObjectID } = require("mongodb");

const id = new ObjectID();
// console.log(id);

const connectionURL = "mongodb://127.0.0.1:27017";

var Web3 = require("web3");
var receiptdata = [];
var _web3 = new Web3(
  new Web3.providers.HttpProvider(
    "https://rinkeby.infura.io/v3/f133272be23d4b718c9e36b693f6d267"
  )
  // Web3.givenProvider
);

const app = express();
const port = 3000;
app.use(express.json()); //parse incoming requests to json

let rawTxns;

MongoClient.connect(
  connectionURL,
  { useNewUrlParser: true },
  (error, client) => {
    if (error) {
      return console.log("Can't connect to db bcoz of: ", error);
    }

    console.log("Connection Successful!");
    const db = client.db("wandx-state-api");

    // db.collection("baskets")
    //   .find({
    //     basketCreationHash:
    //       "0x99447d91a1f3ace64df67ac12e912dcd1ff4b1ae4a1c0557ef9c1c5414523f47"
    //   })
    //   .toArray((error, tasks) => {
    //     if (error) return console.log(error);
    //     console.log(tasks);
    //   });

    db.collection("baskets")
      .find(
        {
          $and: [
            { "tokens.txType.txTypeName": "approve" },
            { "tokens.txType.status.statusName": "pending" }
          ]
        },
        {
          "tokens.txType.signature": 1
        }
      )
      .toArray((error, tasks) => {
        if (error) return console.log(error);
        console.log(tasks);
      });
  }
);

app.post("/addBasket", async (req, res) => {
  const basket = new Basket(req.body);

  try {
    await basket.save();
    res.status(201).send(basket);
  } catch (e) {
    res.status(500).send(e);
  }
});

app.get("/basketByID/:id", async (req, res) => {
  try {
    // console.log(req);
    const basket = await Basket.findOne({
      basketCreationHash: req.params.id
    });
    if (!basket) return res.status(404).send();
    res.status(200).send(basket);
  } catch (e) {
    res.status(500).send(e);
  }
});

app.get("/basketsByUser/:id", async (req, res) => {
  try {
    // console.log(req);
    const baskets = await Basket.find({
      userAddress: req.params.id
    });
    if (baskets.length == 0) return res.status(404).send();
    res.status(200).send(baskets);
  } catch (e) {
    res.status(500).send(e);
  }
});

app.get("/tokensByBasketAndStatus/:id", async (req, res) => {
  try {
    // console.log(req);
    let tokenArray = [];
    const basket = await Basket.findOne({
      basketCreationHash: req.params.id
    });
    if (!basket) return res.status(404).send();

    for (let i = 0; i < basket.tokens.length; ++i) {
      //   // console.log(basket[0].tokens[token]);
      let _token = basket.tokens[i];
      // console.log(req.query.status);
      if (_token.txType.status.statusName == req.query.status) {
        tokenArray.push(_token.tokenSymbol);
      }
      console.log(tokenArray);
    }
    res.send(tokenArray);
  } catch (e) {
    res.status(500).send(e);
  }
});

let updateStatusByHash = async (statusHash, newStatus) => {
  const basket = await Basket.findOneAndUpdate(
    {
      "tokens.txType.status.statusHash": statusHash
    },
    {
      $set: { "tokens.$[elem].txType.status.statusName": newStatus }
    },
    { arrayFilters: [{ "elem.txType.status.statusHash": statusHash }] }
  );
};

let updateTxType = async (statusHash, newTxType) => {
  const basket = await Basket.findOneAndUpdate(
    {
      "tokens.txType.status.statusHash": statusHash
    },
    {
      $set: { "tokens.$[elem].txType.txTypeName": newTxType }
    },
    { arrayFilters: [{ "elem.txType.status.statusHash": statusHash }] }
  );
};

app.post("/updateStatusByHash/:id", async (req, res) => {
  try {
    // console.log(req);
    await updateStatusByHash(req.params.id, req.query.newStatus);
    // if (!basket) return res.status(404).send();

    // await basket.save({ _id: basket._id });
    res.send({ msg: "Status successfully updated" });
  } catch (e) {
    res.status(500).send(e);
  }
});

app.post("/updateTxTypeByHash/:id", async (req, res) => {
  try {
    // console.log(req);
    await updateTxType(req.params.id, req.query.newTxType);
    // if (!basket) return res.status(404).send();

    // await basket.save({ _id: basket._id });
    res.send({ msg: "txType successfully updated" });
  } catch (e) {
    res.status(500).send(e);
  }
});

var d = new Date();
var epoch = d.getTime() / 1000;

var secondsSinceLastTimerTrigger = epoch % 6; // 600 seconds (10 minutes)
var secondsUntilNextTimerTrigger = 6 - secondsSinceLastTimerTrigger;

function continuousSender(rTxns) {
  setTimeout(function() {
    setInterval(sendToBC, 6 * 1000);
    function sendToBC() {
      let txnHash;
      console.log(rTxns);
      _web3.eth
        .sendSignedTransaction(rTxns.rtx1)
        .on("transactionHash", async function(hash) {
          txnHash = hash;
          console.log("hash", hash);
        })
        .on("receipt", async function(receipt) {
          console.log("receipt: ", receipt);
        })
        .on("confirmation", async function(confirmationNumber, receipt) {
          console.log("No: ", confirmationNumber);
          console.log("Receipt: ", receipt);
        })
        .on("error", function(err, receipt) {
          if (err) console.log("error: ", err);
          else console.log("Err receipt", receipt);
        });
    }
  }, secondsUntilNextTimerTrigger * 1000);
}

app.post("/sendToBC", async (req, res) => {
  console.log("sendToBC", typeof req.body);
  // let str = JSON.stringify(req.body);
  let total = Object.keys(req.body).length;
  continuousSender(req.body);
  statusCheckAndUpdate();
  // console.log(req.body.serializedTx);
  // JSON.parse(req.body).forEach(element => {
  // let txnHash;
  // _web3.eth
  //   .sendSignedTransaction(req.body.rtx1)
  // .on("receipt", receipt => {
  //   console.log(receipt, Object.keys(req.body).length);
  //   console.log("receiptdata", receiptdata, receiptdata.length);
  //   if (receipt["status"]) {
  //     receiptdata.push(receipt);
  //     //  res.send(receipt.json())
  //     //res.json({"data":receipt,"status":true});
  //     //res.end();
  //   }
  //   if (total == receiptdata.length) {
  //     console.log("receiptdata", receiptdata);

  //     res.json({ data: receiptdata });
  //     res.end();
  //   }
  // })
  // .on("transactionHash", async function(hash) {
  //   txnHash = hash;
  //   console.log("hash", hash);
  // await updateStatusByHash(txnHash, "pending");
  // })
  // .on("receipt", async function(receipt) {
  //   console.log("receipt: ", receipt);
  // await updateStatusByHash(txnHash, "sent");
  // })
  // .on("confirmation", async function(confirmationNumber, receipt) {
  //   console.log("No: ", confirmationNumber);
  //   console.log("Receipt: ", receipt);
  // await updateStatusByHash(txnHash, "confirmed");
  // await updateTxType(txnHash, "transfer");
  // })
  // .on("error", function(err, receipt) {
  //   if (err) console.log("error: ", err);
  //   else console.log("Err receipt", receipt);
  // });
  // });
  // setTimeout(function() {
  //   let count = 0;
  //   for (var txn in req.body) {
  //     console.log(req.body[txn]);
  //     _web3.eth.getTransaction(req.body[txn]).then(txn => {
  //       // console.log(txn);
  //       if (txn.blockNumber != null) ++count;
  //       console.log("count:", count);
  //       if (total == count) {
  //         res.send({ msg: "All txns confirmed!" });
  //       }
  //     });
  //   }
  // }, 1000);
});

app.listen(port, () => {
  console.log("Server running on port: " + port);
});
